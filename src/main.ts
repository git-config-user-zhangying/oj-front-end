import { createApp } from "vue"
import App from "./App.vue"
import router from "./router"
import Antd from "ant-design-vue"
import "ant-design-vue/dist/reset.css"
import store from "@/store/index"
import "@/utils/request"
// 引入VueCookies
import VueCookies from "vue3-cookies"
// md 编辑器 样式
import "bytemd/dist/index.css"

const app = createApp(App)
// 屏蔽错误信息
app.config.errorHandler = () => null
// 屏蔽警告信息
app.config.warnHandler = () => null

app.use(store).use(Antd).use(router).use(VueCookies).mount("#app")

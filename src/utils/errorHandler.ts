// errorHandler.js
import { message } from 'ant-design-vue';
export default function errorHandler(error:any) {
    message.success({
    content: () => error.message,
    class: 'custom-class',
    style: {
      marginTop: '20vh',
    },
  });
  
}

import axios, { AxiosInstance } from "axios"
import { useCookies } from "vue3-cookies"
// import { useRouter } from "vue-router"
import { message, Modal } from "ant-design-vue"
const { cookies } = useCookies()
// const router =
import router from "@/router/index"
axios.defaults.withCredentials = true
const service: AxiosInstance = axios.create({
  baseURL: "http://127.0.0.1:9527/api",
  timeout: 50000,
  withCredentials: true,
})
// 全局响应拦截器
//响应拦截
service.interceptors.response.use(
  (response) => {
    // const { data: res } = response
    console.log("响应拦截器", response)
    let err_text = ""
    if (response.status === 200) {
      if (response.data.code === 200) {
        return Promise.resolve(response)
      } else {
        // 根据返回的code值来做不同的处理（和后端约定）
        switch (response.data.code) {
          case 40100:
            err_text = response.data.message
            message.error(err_text)
            router.push({
              path: "/user/login",
              // query: { redirect: router.currentRoute.path },
            })

            // window.location.href = "http://127.0.0.1:8080/#/user/login"
            // router.push({ name: "用户登录" })
            break
          default:
            err_text = response.data.message
            message.error(err_text)
        }
        return Promise.reject(response)
      }
    } else {
      return Promise.reject(response)
    }
  },
  (error) => {
    //其他异常
    if (error && error.response) {
      switch (error.response.status) {
        case 400:
          message.error("2")
          break
        default:
          message.error("3")
      }
    }
    return Promise.reject(error) //在axios的catch中写入报错后回调
  }
)

//全局请求拦截器
service.interceptors.request.use(
  (config: any) => {
    // 拦截的业务逻辑
    console.log("请求拦截器", config)
    cookies.set("k1", "v1", "1h")

    return config
  },
  (err: any) => {
    //拦截失败
    return Promise.reject(err)
  }
)
export default service

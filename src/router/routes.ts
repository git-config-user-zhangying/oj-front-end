import { RouteRecordRaw } from "vue-router"
import ACCESS_ENUM from "@/ts/enums/access.enum"
export const routes: Array<RouteRecordRaw> = [
  {
    path: "/user",
    name: "用户",
    children: [
      {
        path: "/user/login",
        name: "用户登录",
        component: import("@/views/user/login.vue"),
      },
      {
        path: "/user/register",
        name: "用户注册",
        component: import("@/views/user/UserRegisterView.vue"),
      },
    ],
    meta: {
      hideInMenu: true,
    },
  },

  {
    path: "/questions",
    name: "浏览题目",
    component: import("@/views/question/QuestionsView.vue"),
  },
  {
    path: "/question_submit",
    name: "浏览题目提交",
    component: import("@/views/question/QuestionSubmitView.vue"),
  },
  {
    path: "/view/question/:id",
    name: "在线做题",
    component: import("@/views/question/ViewQuestionView.vue"),
    props: true,
    meta: {
      access: ACCESS_ENUM.USER,
      hideInMenu: true,
    },
  },
  {
    path: "/add/question",
    name: "创建题目",
    component: import("@/views/question/AddQuestionView.vue"),
    meta: {
      access: ACCESS_ENUM.ADMIN,
    },
  },
  {
    path: "/update/question",
    name: "更新题目",
    component: import("@/views/question/AddQuestionView.vue"),
    meta: {
      access: ACCESS_ENUM.ADMIN,
      hideInMenu: true,
    },
  },
  {
    path: "/manage/question/",
    name: "管理题目",
    component: import("@/views/question/ManageQuestionView.vue"),
    meta: {
      access: ACCESS_ENUM.ADMIN,
    },
  },
  {
    path: "/",
    name: "主页",
    component: import("@/views/question/QuestionsView.vue"),
  },
  {
    path: "/info",
    name: "我的",
    component: import("@/views/user/userInfo.vue"),
    meta: {
      hideInMenu: true,
    },
  },
  {
    path: "/toSubmit",
    name: "统计",
    component: import("@/views/user/submit.vue"),
    meta: {
      hideInMenu: true,
    },
  },
  {
    path: "/noAuth",
    name: "无权限",
    component: import("@/views/NoAuthView.vue"),
    meta: {
      hideInMenu: true,
    },
  },
  {
    path: "/result/404",
    name: "404",
    component: () => import("@/views/404.vue"),
    meta: {
      hideInMenu: true,
    },
  },
  {
    path: "/:pathMatch(.*)",
    redirect: "/result/404",
  },
]

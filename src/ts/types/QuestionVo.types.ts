
import type { UserVO } from "@/ts/types/UserVO.types";
import type { JudgeConfig } from "@/ts/types/JudgeConfig.types";


export type QuestionVo = {
    acceptedNum?: number;
    content?: string;
    createTime?: string;
    favourNum?: number;
    id?: number;
    judgeConfig?: JudgeConfig;
    submitNum?: number;
    tags?: Array<string>;
    thumbNum?: number;
    title?: string;
    updateTime?: string;
    userId?: number;
    userVO?: UserVO;
};

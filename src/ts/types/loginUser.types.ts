export type loginUser = {
  userAccount: string
  userPassword: string
  captchaId: string
}

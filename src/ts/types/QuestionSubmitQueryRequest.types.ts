export type QuestionSubmitQueryRequest = {
  current?: number
  language?: string
  pageSize?: number
  questionId?: number
  sortField?: string
  sortOrder?: string
  status?: number
  userId?: number
}

export type QuestionSubmitAddRequest = {
    code?: string;
    language?: string;
    questionId?: number;
};

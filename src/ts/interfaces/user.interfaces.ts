export interface userInfo {
  id: number
  userName: string
  userAvatar: string
  userProfile: string
  userRole: string
  createTime: Date
  updateTime: Date
}

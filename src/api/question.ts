import request from "../utils/request"
import type { QuestionQueryRequest } from "@/ts/types/QuestionQueryRequest.types"
import type { QuestionSubmitAddRequest } from "@/ts/types/QuestionSubmitAddRequest.types"
export const listQuestionVoByPage = (questionQueryRequest: QuestionQueryRequest) => {
  return request({
    method: "POST",
    url: "/question/list/page/vo",
    data: questionQueryRequest,
  })
}

export const getQuestionVoById = (questionId: any) => {
  return request({
    url: "/question/get/vo",
    method: "GET",
    params: {
      id: questionId,
    },
  })
}

export const addQuestionInfo = (question: any) => {
  return request({
    url: "/question/add",
    method: "POST",
    data: question,
  })
}
export const getQuestionInfoById = (id: Number) => {
  return request({
    url: "/question/get",
    method: "get",
    params: {
      id: id,
    },
  })
}
export const updateQuestionInfo = (questionUpdateRequest: any) => {
  return request({
    url: "/question/update",
    method: "post",
    data: questionUpdateRequest,
  })
}
export const doQuestionSubmitUsingPost = (questionSubmitAddRequest: QuestionSubmitAddRequest) => {
  return request({
    url: "/question/question_submit/do",
    method: "post",
    data: questionSubmitAddRequest,
  })
}
export const deleteQuestionById = (id: Number) => {
  return request({
    url: "/question/delete",
    method: "post",
    data: { id: id },
  })
}

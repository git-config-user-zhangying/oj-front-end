import request from '../utils/request';
export const getKaptcha = () => {
    return request({
        url: '/kaptcha/createImageCode',
        method: 'get',
    });
};

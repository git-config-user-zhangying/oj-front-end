import { loginUser } from "@/ts/types/loginUser.types"
import request from "../utils/request"
export const userLogin = (User: loginUser) => {
  return request({
    url: "/user/outer/login",
    method: "post",
    data: User,
  })
}
export const updateUserInfo = (User: any) => {
  return request({
    url: "/user/outer/update/my",
    method: "post",
    data: User,
  })
}

export const logout = () => {
  return request({
    url: "/user/outer/logout",
    method: "post",
  })
}

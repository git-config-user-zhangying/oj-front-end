import request from "../utils/request"
import type { QuestionSubmitQueryRequest } from "@/ts/types/QuestionSubmitQueryRequest.types"

export const getQuestionSubmitList = (QuestionSubmitQueryRequest: any) => {
  return request({
    url: "/question/question_submit/list/page",
    method: "post",
    data: QuestionSubmitQueryRequest,
  })
}

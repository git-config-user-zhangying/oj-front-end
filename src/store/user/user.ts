import { defineStore } from "pinia"
import { userInfo } from "@/ts/interfaces/user.interfaces"

export const userStore = defineStore("user", {
  persist: true,
  state: () => {
    return {
      user: null as userInfo | null,
    }
  },
  getters: {},
  actions: {
    setUserData(useData: userInfo) {
      this.user = useData
    },
  },
})
